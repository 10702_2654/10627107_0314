package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class Controller {
    public Button btn1;
    public Button btn4;
    public Button btn7;
    public Button btn2;
    public Button btn5;
    public Button btn3;
    public Button btn6;
    public Button btn8;
    public Button btn9;
    public Label monitor;
    public Button btnA;
    public Button btnS;
    public Button btnM;
    public Button btnD;
    public Button btnPoint;
    public int a = 0, b = 0, c = 1, d = 1, sum = 0;
    public Button btnE;

    public void doClick(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        String number = monitor.getText();
        monitor.setText(number.concat(button.getText()));
    }

    public void doCompute(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        switch (button.getText()) {
            case "+":
                a = Integer.parseInt(monitor.getText());
                monitor.setText("");
                break;
            case "-":
                b = Integer.parseInt(monitor.getText());
                monitor.setText("");
                break;
            case "*":
                c = Integer.parseInt(monitor.getText());
                monitor.setText("");
                break;
            case "/":
                d = Integer.parseInt(monitor.getText());
                monitor.setText("");
                break;
            case "=":
                sum = Integer.parseInt(monitor.getText());
                //monitor.setText(String.valueOf(a+b-c*d/sum));
                break;
        }
    }
}
